//
//  ViewController.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-17.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources
import UIKit
import SafariServices

class RootViewController: UIViewController, UITableViewDataSourcePrefetching {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var viewModel: RootViewViewModel?
    
    private let disposeBag = DisposeBag()
    private let cellIdentifier = "cell"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.prefetchDataSource = self
        
        configureBindings()
        viewModel?.fetchActivities()
    }
    
    private func configureBindings() {
        
        viewModel?.querying.emit(to: activityIndicatorView.rx.isAnimating).disposed(by: disposeBag)
        
        viewModel?.activities.drive(onNext: { value in
            _ = value.map { user in
                if self.viewModel?.userCache[user.userId] == nil {
                    self.viewModel?.fetchUser(with: user.userId)
                }
            }
        }).disposed(by: disposeBag)
        
        let dataSource = RxTableViewSectionedReloadDataSource<SectionOfActivities>(
            configureCell: { dataSource, tableView, indexPath, item in
                let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath) as! TableViewCell
                cell.displayMessage(activity: item, userData: self.viewModel?.userCache[item.userId])
                return cell
        })
        
        viewModel?.activities.asObservable().map { activity -> [SectionOfActivities] in
                return [SectionOfActivities(header: "", items: activity)]
            }.bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Activity.self)
            .map { activity in
                let storyboard = UIStoryboard(name: "GoalsViewController", bundle: Bundle.main)
                let vc = storyboard.instantiateViewController(withIdentifier: "GoalsViewController") as! GoalsViewController
                vc.viewModel = GoalsViewViewModel(activity: activity, user: (self.viewModel?.userCache[activity.userId])!)
                return vc
            }
            .subscribe(onNext: { [weak self] viewController in
                self?.navigationController?.pushViewController(viewController, animated: true
                )
                //self?.present(viewController, animated: true, completion: nil)
            })
            .disposed(by: disposeBag)
    }
}

extension RootViewController  {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        let needsFetch: Bool = indexPaths.contains { $0.row >= (viewModel?.numberOfActivities)! - 1 }
        guard let hasMore = viewModel?.hasMore else { return }
        if needsFetch && hasMore {
            if (viewModel?.isFetching)! {
                return
            } else {
                viewModel?.fetchMoreActivities()
            }
        }
    }
}

