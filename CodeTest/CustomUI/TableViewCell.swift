//
//  TableViewCell.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-18.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import UIKit
import Kingfisher

class TableViewCell: UITableViewCell {
    @IBOutlet weak var cellImageView: CustomUIImageView!
    @IBOutlet weak var cellMessageLabel: UILabel!
    @IBOutlet weak var cellAmountLabel: UILabel!
    @IBOutlet weak var cellTimestampLabel: UILabel!
    
    
    func displayMessage(activity: Activity, userData: User?) {
        
        cellAmountLabel.textColor = UIColor.CTColor.LabelColor.CurrencyTextColor
        cellTimestampLabel.textColor = UIColor.CTColor.LabelColor.RegularTextColor
        cellMessageLabel.textColor = UIColor.CTColor.LabelColor.RegularTextColor
        
        cellMessageLabel.attributedText = activity.message.markupParser()
        cellAmountLabel.text = activity.amount.asCurrency()
        cellTimestampLabel.text = activity.timestamp.asMediumString()
        
        if let user = userData {
            guard var comps = URLComponents(string: user.avatarUrl) else { return }
            comps.scheme = "https"
            guard let urlString = comps.string else { return }
            let url = URL(string: urlString)
            let resource = ImageResource(downloadURL: url!, cacheKey: String(user.userId))
            self.cellImageView.kf.setImage(with: resource, options: [.transition(.fade(0.2))], progressBlock: {
                receivedSize, totalSize in
                // perhaps some loading animation?
            }, completionHandler: {
                (image, error, cacheType, imageUrl) in
                if error != nil {
                    self.cellImageView.image = UIImage(named: "NotFound")
                }
            })
        }
    }
}


