//
//  CustomUIImageView.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-23.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation
import UIKit

class CustomUIImageView: UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

