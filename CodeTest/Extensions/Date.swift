//
//  Date.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-22.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation

extension Date {
    func asUTCString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mmZZZ"
        formatter.timeZone = TimeZone.current
        return formatter.string(from: self)
    }
    
    func asMediumString() -> String {
        let formatter = DateFormatter()
        let calendar = Calendar.current
        
        formatter.dateFormat = "dd MMM yyyy"
        formatter.timeZone = TimeZone.current
        
        if calendar.isDateInToday(self) { return "Today" }
        if calendar.isDateInYesterday(self) { return "Yesterday" }
        
        

        return formatter.string(from: self)
    }
}
