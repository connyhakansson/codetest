//
//  String.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-23.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    // This is a simplified version of a Markup parser, this could really be improved
    // but will suffice for this exercise.
    
    func markupParser() -> NSAttributedString {
        
        let regularAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.CTColor.LabelColor.RegularTextColor, .font: UIFont(name: "BentonSans Regular", size: 16.0)!]

        let boldAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.CTColor.LabelColor.BoldTextColor, .font: UIFont(name: "BentonSans Regular", size: 16.0)!]
        
        let attrString = NSMutableAttributedString(string: "", attributes: regularAttributes)
        let spaceString = NSMutableAttributedString(string: " ", attributes: regularAttributes)
        
        let tokens = self.components(separatedBy: " ")
        for token in tokens {
            if let attr = token.slice(from: "<strong>", to: "</strong>") {
                let attrToken = NSMutableAttributedString(string: attr, attributes: boldAttributes)
                attrString.append(attrToken)
                attrString.append(spaceString)
            } else {
                let attrWord = NSMutableAttributedString(string: token, attributes: regularAttributes)
                attrString.append(attrWord)
                attrString.append(spaceString)
            }
        }
        return attrString
    }
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
}
