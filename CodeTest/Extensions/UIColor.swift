//
//  UIColor.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-23.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    public struct CTColor {
        struct LabelColor {
            static let RegularTextColor = UIColor(red: 0.62, green: 0.67, blue: 0.69, alpha: 1)
            static let BoldTextColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.87)
            static let CurrencyTextColor = UIColor(red: 0.35, green: 0.75, blue: 0.04, alpha: 1)
        }
        struct separatorColor {
            static let RegularSeparatorColor = UIColor(netHex: 0xD8D8D8)
        }
    }
}

