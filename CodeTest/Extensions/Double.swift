//
//  Double.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-23.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation

extension Double {
    func asCurrency() -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale.current
        
        formatter.numberStyle = .currency
        return formatter.string(from: self as NSNumber)!
    }
}
