//
//  Activities.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-17.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation

struct Activities: Codable {
    var oldest: Date 
    var activities: [Activity]
}


