//
//  UserRequest.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-22.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation

class UserRequest: APIRequest {
    var method = RequestType.GET
    var path = "users"
    var parameters = [String: String]()
    
    init(params: [String: String]) {
        parameters = params
    }
}
