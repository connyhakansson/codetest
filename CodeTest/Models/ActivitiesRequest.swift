//
//  ActivitiesRequest.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-18.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation

class ActivitiesRequest: APIRequest {
    var method = RequestType.GET
    var path = "activities"
    var parameters = [String: String]()
    
    init(params: [String: String]) {
        parameters = params
    }
}
