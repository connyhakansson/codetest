//
//  SectionOfActivities.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-21.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation
import RxDataSources

struct SectionOfActivities {
    var header: String
    var items: [Activity]
}

extension SectionOfActivities: SectionModelType {
    typealias Item = Activity
    
    init(original: SectionOfActivities, items: [Item]) {
        self = original
        self.items = items
    }
}
