//
//  RootViewViewModel.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-21.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

enum DateTimeConstants {
    static let dateRangeinDays = 14
    static let secondsInAday = 86400
}

class RootViewViewModel {
    
    private let disposeBag = DisposeBag()
    private let apiClient = APIClient()
    
    private let _querying = PublishRelay<Bool>()
    var querying: Signal<Bool> { return _querying.asSignal() }
    
    private let _users = BehaviorRelay<[User]>(value: [])
    var users: Driver<[User]> { return _users.asDriver() }
    
    private let _activities = BehaviorRelay<[Activity]>(value: [])
    var activities: Driver<[Activity]> { return _activities.asDriver() }
    
    var numberOfActivities: Int { return _activities.value.count }
    
    lazy var _dateParameters = BehaviorRelay<[String:String]>(value: ["from":getDateReducedWith(days: currentDateOffsetInDays), "to":getCurrentDate()])
    
    lazy var _idParameters = BehaviorRelay<[String:String]>(value: [:])
    
    var query: Driver<[String:String]> {
        return self._dateParameters.asDriver()
    }

    var userCache: [Int : User] = [:]

    var isFetching: Bool = false
    var currentDateOffsetInDays: Int = DateTimeConstants.dateRangeinDays
    var oldestDate: Date?
    var hasMore: Bool { return (Date.init(timeIntervalSinceNow: -Double(DateTimeConstants.secondsInAday) * Double(currentDateOffsetInDays)) > self.oldestDate!) }
    
    func fetchUser(with id: Int) {
        let params = ["id":String(id)]
        self.userFetch(request: UserRequest(params: params))
    }
    
    private func userFetch(request: APIRequest) {
        apiClient.send(apiRequest: request).asObservable()
            .map { (result: User) -> User in
                self.isFetching = false
                return result
            }.asObservable().subscribe(onNext: { result in
                self.userCache[result.userId] = result
            }).disposed(by: disposeBag)
    }
    
    func fetchActivities() {
        isFetching = true
        _querying.accept(true)
        
        query
        .throttle(0.5)
        .distinctUntilChanged()
        .drive(onNext: { params in
            self.fetch(request: ActivitiesRequest(params: params))
        }).disposed(by: disposeBag)
    }

    func fetchMoreActivities() {
        currentDateOffsetInDays += DateTimeConstants.dateRangeinDays

        // Trigger a new activities fetch (Date driven observer)
        _dateParameters.accept(["from":getDateReducedWith(days: currentDateOffsetInDays), "to":getDateReducedWith(days: currentDateOffsetInDays-DateTimeConstants.dateRangeinDays)])
    }
    
    private func fetch(request: APIRequest) {
        apiClient.send(apiRequest: request).asObservable()
            .map { (result: Activities) -> [Activity] in
                self.oldestDate = result.oldest
                self._querying.accept(false)
                self.isFetching = false
                return result.activities
            }.asObservable().subscribe(onNext: { result in
                let newDataSet = self._activities.value + result
                self._activities.accept(newDataSet)
            }).disposed(by: disposeBag)
        
    }
    
    func getCurrentDate() -> String {
        let formatter = DateFormatter()
        let date = Date()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mmZZZ"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        return formatter.string(from: date)
    }
    
    func getDateReducedWith(days: Int) -> String {
        let numberOfDays = (days == 0) ? 1: days
        let formatter = DateFormatter()
        let date = Date.init(timeIntervalSinceNow: -Double(DateTimeConstants.secondsInAday) * Double(numberOfDays))
        
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mmZZZ"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        return formatter.string(from: date)
    }

}
