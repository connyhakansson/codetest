//
//  GoalsViewController.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-24.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class GoalsViewController: UIViewController {
    
    @IBOutlet weak var displayName: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    var viewModel: GoalsViewViewModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        displayUserGoals()
    }
    
    func displayUserGoals() {
        
        // Could be replaced by subclassing a custom UILabel
        displayName.textColor = UIColor.CTColor.LabelColor.RegularTextColor
        amountLabel.textColor = UIColor.CTColor.LabelColor.CurrencyTextColor
        timestampLabel.textColor = UIColor.CTColor.LabelColor.RegularTextColor
        messageLabel.textColor = UIColor.CTColor.LabelColor.RegularTextColor

        displayName.text = viewModel?.user.displayName ?? "Not known"
        messageLabel.attributedText = viewModel?.activity.message.markupParser()
        timestampLabel.text = viewModel?.activity.timestamp.asMediumString()
        amountLabel.text = viewModel?.activity.amount.asCurrency()
        
        
        if let user = viewModel?.user {
            guard var comps = URLComponents(string: user.avatarUrl) else { return }
            comps.scheme = "https"
            guard let urlString = comps.string else { return }
            let url = URL(string: urlString)
            let resource = ImageResource(downloadURL: url!, cacheKey: String(user.userId))
            self.avatarImageView.kf.setImage(with: resource, options: [.transition(.fade(0.2))], progressBlock: {
                receivedSize, totalSize in
                // perhaps some loading animation?
            }, completionHandler: {
                (image, error, cacheType, imageUrl) in
                if error != nil {
                    self.avatarImageView.image = UIImage(named: "NotFound")
                }
            })
        }
    }
    
    
}

