//
//  GoalsViewViewModel.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-24.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import Foundation

class GoalsViewViewModel {
    let activity: Activity
    let user: User
    
    init (activity: Activity, user: User) {
        self.activity = activity
        self.user = user
    }
}
