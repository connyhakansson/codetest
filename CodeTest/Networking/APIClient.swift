//
//  APIClient.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-17.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import RxSwift
import RxCocoa
import Foundation

enum CustomErrors: Error {
    case noData
    case badUrl
}

class APIClient {
    private let baseURL = URL(string: "https://qapital-ios-testtask.herokuapp.com")!
    
    func send<T: Codable>(apiRequest: APIRequest) -> Observable<T> {
        return Observable.create { observer in
            let request = apiRequest.request(with: self.baseURL)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    print("Error: \(error)")
                    observer.onError(error)
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .iso8601
                    let model: T = try decoder.decode(T.self, from: data!)
                    observer.onNext(model)
                } catch let error {
                    print("Could not retreive JSON")
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            task.resume()
            return Disposables.create {
                task.cancel()
            }
        }
    }
}

