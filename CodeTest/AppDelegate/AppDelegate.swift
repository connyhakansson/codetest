//
//  AppDelegate.swift
//  CodeTest
//
//  Created by Conny Hakansson on 2019-01-17.
//  Copyright © 2019 Tobo netSolution AB. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // This would be better of with a DI Factory.
        
        if let vc = window?.rootViewController as? UINavigationController {
            if let firstVC = vc.viewControllers.first as? RootViewController {
                firstVC.viewModel = RootViewViewModel()
            }
        }
        
        return true
    }

}

